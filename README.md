# msts2unix
conversion of microsoft NT timestamp to unix timestamp

needs perl module DateTime::Format::Epoch::Unix

syntax

```
msts2unix {some ms nt timestamp}
```
